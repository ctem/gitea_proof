This is an [OpenPGP proof] that connects [my OpenPGP key] to [this Codeberg account].

[openpgp proof]: https://keyoxide.org/guides/openpgp-proofs
[my openpgp key]: https://keyoxide.org/5F30852EAEDD2A97D4C16543124146224E3EB89F
[this codeberg account]: https://codeberg.org/ctem
